App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  hasVoted: false,
  assembly:0,

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // TODO: refactor conditional
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // Specify default instance if no web3 instance provided
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  initContract: function() {
    $.getJSON("/Election.json", function(election) {
      // Instantiate a new truffle contract from the artifact
      App.contracts.Election = TruffleContract(election);
      // Connect provider to interact with contract
      App.contracts.Election.setProvider(App.web3Provider);


      return App.render();
    });
  },

  render: function(data) {
    
    var electionInstance;

    // Load account data
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        //App.account = web3.eth.accounts[0];
        App.account = '0xb7775Ab9E5607ee2D3FFc95651B857A47a112C0A';
        // alert(App.account);
        // alert(window.sessionStorage.user_data);
        // App.account = window.sessionStorage.user_data;
      }
    });
    //console.log(web3.eth.accounts);
    // Load contract data
    App.contracts.Election.deployed().then(function(instance) {
      electionInstance = instance;
      //alert("1");
      return electionInstance.candidatesCount();
    }).then(function(candidatesCount) {
      

      var votingResults = $('#votingResults');
      votingResults.empty();

      App.assembly = $('#assemblySelect').val();

      for (var i = 1; i <= candidatesCount; i++) {
        //alert(i);
        electionInstance.candidates(i).then(function(candidate) {
          var id = candidate[0];
          var name = candidate[1];
          var img = candidate[2];
          var assembly = candidate[3];
          var voteCount = candidate[4];
          //Enable when duplicate list is rendered
          if(id==1){
            votingResults.empty();
          }

          if(assembly==App.assembly){
            // Render candidate ballot option
            var template = "<tr><td>"+ id + "</td><td>" + name + "</td>";
            template += "<td><img src='http://localhost:8080/img/candidate/"+img+"' width=60/></td></div>"
            template += "<td>" + voteCount + "</td>";
            votingResults.append(template);
          }

        });
      }
      
      return electionInstance.voters(App.account);
    }).then(function(hasVoted) {
      // Do not allow a user to vote
      if(hasVoted) {
      }
    }).catch(function(error) {
      console.warn(error);
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});