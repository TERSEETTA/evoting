App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  hasVoted: false,

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // TODO: refactor conditional
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // Specify default instance if no web3 instance provided
      console.log("Checking manual");
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  initContract: function() {
    console.log("calling json");
    $.getJSON("/Election.json", function(election) {
      // Instantiate a new truffle contract from the artifact
      App.contracts.Election = TruffleContract(election);
      // Connect provider to interact with contract
      App.contracts.Election.setProvider(App.web3Provider);

      //App.listenForEvents();
      console.log("contract init");
      return App.render();
    });
  },

  // Listen for events emitted from the contract
  listenForEvents: function() {
    App.contracts.Election.deployed().then(function(instance) {
      instance.votedEvent({}, {
        fromBlock: 'latest',
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)
        // Reload when a new vote is recorded
        App.render(1);
      });
    });
  },

  render: function(data) {
    // if(data!=1)
    //   return;
    var electionInstance;
    console.log("render");
    // Load account data
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        // App.account = web3.eth.accounts[0];
        App.account = '0xa31693cF5Bc4Fa6f165221A673109C27B8A99aa0';
        // alert(App.account);
        // alert(window.sessionStorage.user_data);
        // App.account = window.sessionStorage.user_data;
      }
    });
    //console.log(web3.eth.accounts);
    // Load contract data
    App.contracts.Election.deployed().then(function(instance) {
      electionInstance = instance;
      //alert("1");
      return electionInstance.candidatesCount();
    }).then(function(candidatesCount) {
      

      // var candidatesSelect = $('#candidatesSelect');
      // candidatesSelect.empty();

      for (var i = 1; i <= candidatesCount; i++) {
        //alert(i);
        electionInstance.candidates(i).then(function(candidate) {
          var id = candidate[0];
          var name = candidate[1];
          var voteCount = candidate[3];
          //Enable when duplicate list is rendered
          // if(id==1){
          //   candidatesResults.empty();
          //   candidatesSelect.empty();
          // }
          // Render candidate Result
          // var candidateTemplate = "<tr><th>" + id + "</th><td>" + name + "</td><td>" + voteCount + "</td></tr>"
          // candidatesResults.append(candidateTemplate);

        });
      }
      
      return electionInstance.voters(App.account);
    }).then(function(hasVoted) {
      // Do not allow a user to vote
      if(hasVoted) {
      }
    }).catch(function(error) {
      console.warn(error);
    });
  },

  publishCandidate: function(id, name, symbol, assembly) {
    if(!confirm('Are you sure to publish this candidate : '+name)){
      return;
    }
    App.contracts.Election.deployed().then(function(instance) {
      balance = web3.eth.getBalance(App.account);
      return instance.addCandidate(name,symbol,assembly, { from: App.account, gas:3000000 });
    }).then(function(result) {
      // Wait for votes to update
      $.get("/publish?id="+id, function(data, status){
        if(data=='Updated'){
          alert('Updated');
          location.reload();
        }
      });
      console.log(result);
      // $("#content").hide();
      // $("#loader").show();
    }).catch(function(err) {
      console.error(err);
    });
  }
};

$(function() {
  $(window).load(function() {
    console.log("initialising");
    App.init();
  });
});