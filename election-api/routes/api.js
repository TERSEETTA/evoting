var express = require('express');
var sqlite3 = require('sqlite3').verbose();
var router = express.Router();

/* GET admin listing. */
router.get('/', function(req, res, next) {
  console.log('API request');
  var response = {
    data:'',
    status:0
  }
  var db = new sqlite3.Database('mydb.db');
  console.log('API DATABASE LOAD');
  //Get aadhar number and biometric XML
  var data = req.query.data;
  var callback = req.query.callback;
  var biometric = req.query.biometric;
  var parseString = require('xml2js').parseString;
  var xml = biometric;
  // var crypto = require("crypto");
  // var rand = crypto.randomBytes(20).toString('hex');

  //Parse XML to retrieve biometric data
  parseString(xml, function (err, result) {
    if (err) {
      throw err;
    }
    biometric = result.PidData.DeviceInfo[0].$.mc;
    console.log('API GET REQUEST'+data);
    //Match aadhar number from db
    db.get("SELECT * FROM voters WHERE aadhar = ?",[data], function(err, row) {
      
      if (err) {
        throw err;
      }
      //Retrieve corresponding assembly number
      db.get("SELECT * FROM booth WHERE b_id = ?",[row.booth], function(err, booth) {
        
        if (err) {
          throw err;
        }
        if(row && biometric == row.biometric){
          //Set voter authentication verified
          gender = row.id%2;
          db.run('UPDATE voters SET gender = ? WHERE id = ?', [gender,row.id], (err) => {
            if(err) {
              return console.log(err.message); 
            }
          });
          response.user=data;
          response.name=row.name;
          response.key=row.key;
          response.assembly=booth.assembly;
          response.status=1;
        }
        res.send(callback+"("+JSON.stringify(response)+");");
      });
    });
  });
});


router.get('/voted', function(req, res, next) {
  console.log('API request');
  var response = {
    data:'',
    status:0
  }
  var db = new sqlite3.Database('mydb.db');
  console.log('API DATABASE LOAD');
  //Get aadhar number and biometric XML
  var data = req.query.data;
  var callback = req.query.callback;
  
  db.run('UPDATE voters SET verified = 1 WHERE aadhar = ?', [data], (err) => {
    if(err) {
      return console.log(err.message); 
    }
    response.status=1;
    res.send(callback+"("+JSON.stringify(response)+");");
  });
});
module.exports = router;
