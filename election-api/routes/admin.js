var express = require('express');
var sqlite3 = require('sqlite3').verbose();
var multer  = require('multer');
var path = require('path');
var router = express.Router();

var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
      cb(null, './public/img/candidate/')
  },
  filename: function (req, file, cb) {
      var datetimestamp = Date.now();
      cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
  }
});

var upload = multer({ //multer settings
  storage: storage,
  fileFilter: function (req, file, callback) {
      var ext = path.extname(file.originalname);
      if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
          return callback(new Error('Only images are allowed'))
      }
      callback(null, true)
  },
  limits:{
      fileSize: 256 * 256
  }
}).single('symbol');

router.get('/register', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');s
  var a_id = req.query.assembly?req.query.assembly:1;
  
  db.all('SELECT * FROM candidates WHERE assembly = ?', [a_id], (err, rows) => {
    if (err) {
      throw err;
    }
    db.all('SELECT * FROM assembly', [], (err, lists) => {
      if (err) {
        throw err;
      }
      res.render('register', { 
        page: 'register', 
        title: 'Add candidate',
        msg:false,
        type:0,
        text:'',
        data:rows,
        assembly:lists,
        choosen:a_id
      });
    });
  });
});

router.post('/register', upload, function (req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  var response = {msg:false,type:0,text:''};
  var a_id = req.query.assembly?req.query.assembly:1;
  db.get("SELECT * FROM candidates where c_name like ? AND assembly = ?",[req.body.cname,req.body.assembly], function(err, row) {
    if (err) {
      throw err;
    }
    if(row){      
      response.msg=true;
      response.type=3;
      response.text='Same candidate details found!';
    }else{
      response.msg=true;
      response.type=2;
      response.text='Saved!';
    }
    db.run('INSERT INTO candidates(c_id, c_name, c_symbol, assembly, status) VALUES(NULL, ?, ?, ?, 0)', [req.body.cname,req.file.filename,req.body.assembly], (err) => {
      if(err) {
        return console.log(err.message); 
      }
      db.all('SELECT * FROM candidates WHERE assembly = ?', [a_id], (err, rows) => {
        if (err) {
          throw err;
        }
        db.all('SELECT * FROM assembly', [], (err, lists) => {
          if (err) {
            throw err;
          }
          res.render('register', { 
            page: 'register', 
            title: 'Add candidate',
            msg:response.msg,
            type:response.type,
            text:response.text,
            data:rows,
            assembly:lists,
            choosen:a_id
          });
        });
      });
    })
  });
})

router.get('/delete_candidate', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');

  var id = req.query.id;
  if(id){
    db.run('DELETE FROM candidates WHERE c_id = ?', [id], (err) => {
      if (err) {
        throw err;
      }
    });
  }
  res.redirect('/admin/register')
});

router.get('/reg_assembly', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');
  
  db.all('SELECT * FROM assembly', [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.render('reg-assembly', { 
      page: 'reg-booth', 
      title: 'Add Data',
      msg:false,
      type:0,
      text:'',
      data:rows
    });
  });
});

router.post('/reg_assembly', function (req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  var response = {msg:false,type:0,text:''};
  db.get("SELECT * FROM assembly where a_name like ?",[req.body.aname], function(err, row) {
    if (err) {
      throw err;
    }
    if(row){      
      response.msg=true;
      response.type=4;
      response.text='Same assembly name found!';
    }else{
      response.msg=true;
      response.type=2;
      response.text='Saved!';

      db.run('INSERT INTO assembly(a_id, a_name) VALUES(NULL, ?)', [req.body.aname], (err) => {
        if(err) {
          return console.log(err.message); 
        }
        db.all('SELECT * FROM assembly', [], (err, rows) => {
          if (err) {
            throw err;
          }
          res.render('reg-assembly', { 
            page: 'reg-booth', 
            title: 'Add assembly',
            msg:response.msg,
            type:response.type,
            text:response.text,
            data:rows
          });
        });
      })
    }
  });
})

router.get('/reg_booth', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');
  var response = {msg:false,type:0,text:''};
  var a_id = req.query.assembly?req.query.assembly:1;
  var stat = req.query.stat;
  if(stat==1){
    response.msg=true;
    response.type=2;
    response.text='Saved!';
  }else if(stat==2){
    response.msg=true;
    response.type=4;
    response.text='Same booth name found!';
  }
  
  db.all('SELECT * FROM booth WHERE assembly = ?', [a_id], (err, rows) => {
    if (err) {
      throw err;
    }
    db.all('SELECT * FROM assembly', [], (err, lists) => {
      if (err) {
        throw err;
      }
      res.render('reg-booth', { 
        page: 'reg-booth', 
        title: 'Add Data',
        msg:response.msg,
        type:response.type,
        text:response.text,
        data:rows,
        assembly:lists,
        choosen:a_id
      });
    });
  });
});

router.post('/reg_booth', function (req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  var a_id = req.query.assembly?req.query.assembly:1;

  db.get("SELECT * FROM booth where b_name like ? AND assembly = ?",[req.body.bname,req.body.assembly], function(err, row) {
    if (err) {
      throw err;
    }
    if(row){      
      res.redirect('/admin/reg_booth?stat=2')
    }else{
      db.run('INSERT INTO booth(b_id, b_name, assembly) VALUES(NULL, ?, ?)', [req.body.bname,req.body.assembly], (err) => {
        if(err) {
          return console.log(err.message); 
        }
        res.redirect('/admin/reg_booth?stat=1')
      })
    }
  });
})

router.get('/list', function(req, res, next) {
  // res.setHeader('Access-Control-Allow-Origin', '*');
  // res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  // res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');
  var id = req.query.id;
  var callback = req.query.callback;
  
  db.all('SELECT * FROM candidates left join assembly on candidates.assembly = assembly.a_id', [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.render('list-candidates', { 
      page: 'list', 
      title: 'Candidate List',
      msg:false,
      type:0,
      text:'',
      data:rows
    });
  });
});

router.get('/results', function(req, res, next) {

  var db = new sqlite3.Database('mydb.db');
  var a_id = req.query.assembly?req.query.assembly:1;
  db.all('SELECT * FROM assembly', [], (err, lists) => {
    if (err) {
      throw err;
    }
    res.render('voting-results', { 
      page: 'results', 
      title: 'Voting Results',
      msg:false,
      type:0,
      text:'',
      assembly:lists,
      choosen:a_id
    });
  });
});

router.get('/report_assembly', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');
  
  db.all(`SELECT a1.a_name,Count(*) ttl,
            (SELECT Count(*)
            FROM   voters
                    LEFT JOIN booth
                          ON booth.b_id = voters.booth
            WHERE  voters.verified = 1
                    AND booth.assembly = b1.assembly) ttl_vtd,
            (SELECT Count(*)
            FROM   voters
                    LEFT JOIN booth
                          ON booth.b_id = voters.booth
            WHERE  voters.gender = 1
                    AND booth.assembly = b1.assembly) ml,
            (SELECT Count(*)
            FROM   voters
                    LEFT JOIN booth
                          ON booth.b_id = voters.booth
            WHERE  voters.gender = 1
                    AND voters.verified = 1
                    AND booth.assembly = b1.assembly) ml_vtd,
            (SELECT Count(*)
            FROM   voters
                    LEFT JOIN booth
                          ON booth.b_id = voters.booth
            WHERE  voters.gender = 0
                    AND booth.assembly = b1.assembly) fm,
            (SELECT Count(*)
            FROM   voters
                    LEFT JOIN booth
                          ON booth.b_id = voters.booth
            WHERE  voters.gender = 0
                    AND voters.verified = 1
                    AND booth.assembly = b1.assembly) fm_vtd
          FROM   voters v1
            LEFT JOIN booth b1
                  ON b1.b_id = v1.booth
            LEFT JOIN assembly a1
                  ON a1.a_id = b1.assembly
          GROUP  BY b1.assembly`, [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.render('report-assembly', { 
      page: 'report', 
      title: 'Reports',
      msg:false,
      type:0,
      text:'',
      data:rows
    });
  });
});


router.get('/report_booth', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');
  var a_id = req.query.assembly?req.query.assembly:1;
  
  db.all('SELECT * FROM assembly', [], (err, lists) => {
    if (err) {
      throw err;
    }
    db.all(`SELECT b1.b_name,Count(*) ttl,
              (SELECT Count(*)
              FROM   voters
              WHERE  voters.verified = 1
                      AND voters.booth = b1.b_id) ttl_vtd,
              (SELECT Count(*)
              FROM   voters
              WHERE  voters.gender = 1
                      AND voters.booth = b1.b_id) ml,
              (SELECT Count(*)
              FROM   voters
              WHERE  voters.gender = 1
                      AND voters.verified = 1
                      AND voters.booth = b1.b_id) ml_vtd,
              (SELECT Count(*)
              FROM   voters
              WHERE  voters.gender = 0
                      AND voters.booth = b1.b_id) fm,
              (SELECT Count(*)
              FROM   voters
              WHERE  voters.gender = 0
                      AND voters.verified = 1
                      AND voters.booth = b1.b_id) fm_vtd
          FROM   voters v1
              LEFT JOIN booth b1
                    ON b1.b_id = v1.booth
          WHERE b1.assembly = ?              
          GROUP  BY b1.b_id `,[a_id], (err, rows) => {
      if (err) {
        throw err;
      }
      res.render('report-booth', { 
        page: 'report', 
        title: 'Reports',
        msg:false,
        type:0,
        text:'',
        assembly:lists,
        data:rows,
        choosen:a_id
      });
    });
  });
});


router.get('/voters', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  console.log(req.session);
  //if(!req.session)
    //res.redirect('/');
  
  db.all('SELECT * FROM voters', [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.render('voters', { 
      page: 'voters', 
      title: 'Voters',
      msg:false,
      type:0,
      text:'',
      data:rows
    });
  });
});

module.exports = router;
